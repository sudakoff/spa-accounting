import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

let store = new Vuex.Store({
  state: {
    users: [],
    users_title: []
  },
  mutations: {
    SET_USERS_TO_STATE: (state, users) => {
      const alluser = users.map((obj) => {
        obj.profession = "unknown";
        return obj;
      })
      state.users = alluser

      // const alltitle = users.map((obj) => obj.name.title)
      // const allfirst = users.map((obj) => obj.name.first)
      // const alllast = users.map((obj) => obj.name.last)
      // state.users_title = [...alltitle,...allfirst,...alllast]
    },
    REMOVE_USER: (state, index) => {
      state.users.splice(index, 1)
    },
    SET_USER: (state, user) => {
      const newUser = {
          profession: "unknown",
          gender: "female",
          name: {
          title: user.gender ? "Mrs" : "Mr",
          first: user.first,
          last: user.last
          },
          location: {
          street: {
          number: null,
          name: ""
          },
          city: "",
          state: "",
          country: "",
          postcode: null,
          coordinates: {
          latitude: "",
          longitude: ""
          },
          timezone: {
          offset: "",
          description: ""
          }
          },
          email: "",
          login: {
          uuid: Date.now(),
          username: "",
          password: "",
          salt: "",
          md5: "",
          sha1: "",
          sha256: ""
          },
          dob: {
          date: "",
          age: new Date().getFullYear() - user.selectedDate.getFullYear()
          },
          registered: {
          date: Date(),
          age: null
          // Date().getFullYear() - date
          },
          phone: "",
          cell: "",
          id: {
          name: "",
          value: ""
          },
          picture: {
          large: "http://placekitten.com/128/128",
          medium: "http://placekitten.com/72/72",
          thumbnail: "http://placekitten.com/48/48"
          },
          nat: ""
          }

      state.users.unshift(newUser)
    },
    SEARCH_USER:() => {
      console.log(this.search.text);
      // console.log(this.userList_actual);
      console.log(this.state.users);
      // var inside = this;
      // state.users = state.users.filter(function(user) {
      //   if (user.name.first
      //     .toLowerCase()
      //     .indexOf(inside.search.text.toLowerCase()) != "-1") {
      //       return user;
      //     }
      // });
    }
    // EDIT_USER: (state, user) => {
    //   state.users = [...state.user, this.inputs: 123 ]
    // }
  },
  actions: {
    GET_USERS({commit}) {
      if (store.state.users.length == 0) {
        return axios.get('https://randomuser.me/api/?results=6')
      .then((users) => {
        commit('SET_USERS_TO_STATE', users.data.results)
        return users
      })
      .catch((error) => {
        console.log(error)
      })
      }
    },
    DELETE_USER({commit}, index) {
      commit('REMOVE_USER', index)
    },
    ADD_USER: (context, user) => {
      context.commit("SET_USER", user)
    },
    SAVE_USER: (context, user) => {
      context.commit("EDIT_USER", user)
    },
    SEARCH_TEXT: (context) => {
      context.commit('SEARCH_USER')
    }
  },
  getters: {
    USERS(state) {
      return state.users
    }
  }
});

export default store
