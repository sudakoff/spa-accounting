import Vue from 'vue'
import Router from 'vue-router'

import vContactsList from '../components/v-contacts-list'
import vProfileInfo from '../components/v-profile-info'


Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'contacts-list',
      component: vContactsList
    },
    {
      path: '/profile',
      name: 'profile',
      component: vProfileInfo,
      props: true
    }
  ]
})

router.beforeEach((to, from, next) => {
  let title = to.name;
  const keys = Object.keys(to.params);
  if (keys.length) {
    title = `${to.name}: ${to.params[keys[0]]}`;
    if (to.params[keys[1]]) {
      title += ` ${to.params[keys[1]]}`;
    }
  }
  document.title = title;
  next();
});

export default router
