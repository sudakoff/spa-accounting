# SPA accounting

Vue (Vuex + Router + Axios + 98.css)

### Web app
See [DEMO](https://sudakoff.gitlab.io/spa-accounting).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
